import 'dart:html';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/src/material/outlined_button.dart';

import 'package:flutter/material.dart';

class SessionList extends StatefulWidget {
  const SessionList({Key? key}) : super(key: key);

  @override
  State<SessionList> createState() => _SessionListState();
}

class _SessionListState extends State<SessionList> {
  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection("sessions").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _nameFieldController = TextEditingController();
    TextEditingController _locationFieldController = TextEditingController();
    TextEditingController _dateFieldController = TextEditingController();
    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("sessions")
          .doc(docId)
          .delete()
          .then((value) => print("Record deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("sessions");
      _nameFieldController.text = data["Name"];
      _locationFieldController.text = data["Location"];
      _dateFieldController.text = data["Date"];
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Update"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: _nameFieldController,
                  ),
                  TextField(
                    controller: _locationFieldController,
                  ),
                  TextField(
                    controller: _dateFieldController,
                  ),
                  TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"]).update({
                          "Name": _nameFieldController.text,
                          "Location": _locationFieldController.text,
                          "Date": _dateFieldController.text,
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"))
                ]),
              ));
    }

    return StreamBuilder(
      stream: _mySessions,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data() as Map<String, dynamic>;
                          return Column(children: [
                            Card(
                                child: Column(children: [
                              ListTile(
                                title: Text(data['Name']),
                                subtitle: Text(data['Location']),
                              ),
                              ListTile(
                                subtitle: Text(data['Date']),
                              ),
                              ButtonTheme(
                                  child: ButtonBar(children: [
                                OutlinedButton.icon(
                                  onPressed: () {
                                    _update(data);
                                  },
                                  icon: Icon(Icons.edit),
                                  label: Text("Edit"),
                                ),
                                OutlinedButton.icon(
                                  onPressed: () {
                                    _delete(data["doc_id"]);
                                  },
                                  icon: Icon(Icons.remove),
                                  label: Text("Delete"),
                                )
                              ])),
                            ]))
                          ]);
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (Text("No data"));
        }
      },
    );
  }
}
