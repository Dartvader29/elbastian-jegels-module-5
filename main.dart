// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:module_4/addsession.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyAiSgmmr00D0jxE4XQmJhTHfa5wLPR5g-k",
          authDomain: "myapp1-89d73.firebaseapp.com",
          projectId: "myapp1-89d73",
          storageBucket: "myapp1-89d73.appspot.com",
          messagingSenderId: "1008435277881",
          appId: "1:1008435277881:web:a4c57d8a301629f5189de8",
          measurementId: "G-YFPT4JVD54"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "EJ's App",
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: const MyHomePage(title: 'Welcome to my App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("EJ's App")),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[AddSession()],
        )));
  }
}
